QEMU: smb daemon gets additional command line parameters from env variable

The new version 4 of Samba disables version 1 SMB protocols, now to
run old Win clients we must modify the temporary configuration of the
samba daemon to enable protocols before starting the samba client.

This patch gets the value of the environment variable SMBDOPTIONS and
appends it to the smbd command line; it allows the user to specify
additional samba daemon parameters before starting qemu.

It also patches the fork_exec function in misc.c, to parse correctly
command lines that contain spaces.

Example:

Set the environment variable SMBDOPTIONS before executing qemu.

export SMBDOPTIONS="--option='server min protocol=CORE' -d 4"

