busybox patches.

* special PATH %applet
    busybox will look for internal applets when a path name %applet
    appears in PATH.
    Scenarios:
    - if there is no %applet in PATH then busybox will look for
      internal applets as last resource to execute a command.
    - PATH=%applet:/usr/bin:... , following the PATH sequence
      busybox looks for an executable or an internal applet

* klibc-utils/resume.c
    resume from file, write offset to /sys/power/resume_offset
    
