libnotify

  * Write notification ID to standard output.
  * Option -r,--reuse-id to reuse a message window by ID.
  * Update man page.
